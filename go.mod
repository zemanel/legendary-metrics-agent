module gitlab.com/zemanel/legendary-metrics-agent

go 1.15

require (
	github.com/DataDog/datadog-go v4.2.0+incompatible
	github.com/go-co-op/gocron v0.3.3
	github.com/mitchellh/go-homedir v1.1.0
	github.com/shirou/gopsutil/v3 v3.20.10
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
)
