package collectors

import (
	"github.com/shirou/gopsutil/v3/process"
)

// ProcessInfoCollector an interface for utility to get current running processes
type ProcessInfoCollector interface {
	GetRunningProcesses() (runningProcesses []*process.Process, err error)
}

// GoPsUtilProcessInfoCollector Collect process info
type GoPsUtilProcessInfoCollector struct {
}

// GetRunningProcesses returns data about current running processes
func (c GoPsUtilProcessInfoCollector) GetRunningProcesses() (runningProcesses []*process.Process, err error) {
	runningProcesses, err = process.Processes()
	return
}

// NewGoPsUtilProcessInfoCollector returns a new GoPsUtilProcessInfoCollector
func NewGoPsUtilProcessInfoCollector() *GoPsUtilProcessInfoCollector {
	return &GoPsUtilProcessInfoCollector{}
}
