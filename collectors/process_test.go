package collectors

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// Test_NewGoPsUtilProcessInfoCollector tests listing current processes
func Test_NewGoPsUtilProcessInfoCollector(t *testing.T) {
	c := NewGoPsUtilProcessInfoCollector()
	processes, err := c.GetRunningProcesses()
	assert.NoError(t, err)
	assert.True(t, len(processes) > 0)
}
