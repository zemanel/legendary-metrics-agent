package config

// AgentConfig Holder for agent configuration values
type AgentConfig struct {
	// Path of PID file
	PIDFile string

	// address of statsd server
	StatsdServerAddress string

	// address  of statsd server
	StatsdClientPrefix string

	// Metrics polling interval
	MetricsPollingInterval uint64
}
