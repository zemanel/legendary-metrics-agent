package scheduler

import (
	"time"

	"github.com/go-co-op/gocron"
)

// TaskScheduler is an interface for a testable background job scheduler
type TaskScheduler interface {
	RunAtIntervalSeconds(interval uint64, jobFun interface{})
	Start()
}

// BlockingTaskScheduler is a scheduler that blocks current thread and runs tasks in background
type BlockingTaskScheduler struct {
	scheduler *gocron.Scheduler
}

//RunAtIntervalSeconds runs a function at a timed secondsinterval
func (s BlockingTaskScheduler) RunAtIntervalSeconds(interval uint64, jobFun interface{}) {
	s.scheduler.Every(interval).Seconds().Do(jobFun)
}

//Start starts executing scheduled jobs
func (s BlockingTaskScheduler) Start() {
	s.scheduler.StartBlocking()
}

// NewBlockingTaskScheduler returns a new non blocking scheduler that runs jobs when Start() is called
func NewBlockingTaskScheduler() *BlockingTaskScheduler {
	return &BlockingTaskScheduler{scheduler: gocron.NewScheduler(time.UTC)}
}

// NonBlockingTaskScheduler is a scheduler that blocks current thread and runs tasks in background
type NonBlockingTaskScheduler struct {
	scheduler *gocron.Scheduler
}

//RunAtIntervalSeconds runs a function at a timed secondsinterval
func (s NonBlockingTaskScheduler) RunAtIntervalSeconds(interval uint64, jobFun interface{}) {
	s.scheduler.Every(interval).Seconds().Do(jobFun)
}

//Start starts executing scheduled jobs
func (s NonBlockingTaskScheduler) Start() {
	s.scheduler.RunAll()
}

// NewNonBlockingTaskScheduler returns a new blocking scheduler
func NewNonBlockingTaskScheduler() *NonBlockingTaskScheduler {
	return &NonBlockingTaskScheduler{scheduler: gocron.NewScheduler(time.UTC)}
}
