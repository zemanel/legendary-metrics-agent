package main

import "gitlab.com/zemanel/legendary-metrics-agent/cmd"

func main() {
	cmd.Execute()
}
