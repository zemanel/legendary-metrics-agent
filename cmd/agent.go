package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"gitlab.com/zemanel/legendary-metrics-agent/agent"
	"gitlab.com/zemanel/legendary-metrics-agent/collectors"
	"gitlab.com/zemanel/legendary-metrics-agent/config"
	"gitlab.com/zemanel/legendary-metrics-agent/scheduler"

	"github.com/spf13/viper"
)

const (
	// The name of our config file, without the file extension because viper supports many different config file languages.
	configDefaultFilename = ".legendary-metrics-agent"

	// Environment variable prefix of all config environment variables
	configEnvPrefix = "LEGENDARY_METRICS_AGENT"
)

var (
	// Config file path
	cfgFile string

	// startAgentCmd starts the metrics collector agent
	startAgentCmd = NewAgentCmd()
)

// startAgentCommand starts the metrics agent when root command is executed
func startAgentCommand(cmd *cobra.Command, args []string) error {
	// Agent configuration (which is read for CLI flags/environment variables)
	agentConfig := config.AgentConfig{}

	agentConfig.StatsdServerAddress = viper.GetString("statsd-server-address")
	agentConfig.StatsdClientPrefix = viper.GetString("statsd-client-prefix")
	agentConfig.MetricsPollingInterval = viper.GetUint64("metrics-polling-interval")

	taskScheduler := scheduler.NewBlockingTaskScheduler()
	processInfoCollector := collectors.NewGoPsUtilProcessInfoCollector()

	agent, err := agent.New(&agentConfig, cmd.OutOrStdout(), taskScheduler, processInfoCollector)

	if err != nil {
		return err
	}

	if err := agent.Start(); err != nil {
		return err
	}
	return nil
}

// NewAgentCmd creates and return a new agentCmd instance
func NewAgentCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "legendary-metrics-agent",
		Short:         "Start metrics agent",
		SilenceUsage:  true,
		SilenceErrors: true,
		RunE:          startAgentCommand,
	}

	// Flags and configuration settings.
	cmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.legendary-metrics-agent.yaml)")

	cmd.Flags().String("statsd-server-address", "localhost:9125", "statsd UDP address")
	cmd.Flags().String("statsd-client-prefix", "legendary_metrics.", "Statsd client prefix (optional)")
	cmd.Flags().Int("metrics-polling-interval", 1, "Polling interval for metrics in seconds")

	viper.BindPFlag("statsd-server-address", cmd.Flags().Lookup("statsd-server-address"))
	viper.BindPFlag("statsd-client-prefix", cmd.Flags().Lookup("statsd-client-prefix"))
	viper.BindPFlag("metrics-polling-interval", cmd.Flags().Lookup("metrics-polling-interval"))

	return cmd
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the startAgentCmd.
func Execute() {
	if err := startAgentCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".legendary-metrics-agent" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(configDefaultFilename)
	}

	// read in environment variables that match
	viper.SetEnvPrefix(configEnvPrefix)
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)
	viper.AutomaticEnv()

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
