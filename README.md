# legendary-metrics-agent

An service for collecting per process used memory of a Linux host written in [Golang](https://golang.org).
The collected information is be output to a suitable metrics back-end via statsd (TICK, prometheus, statsite).

## Design considerations


[Golang](https://golang.org) was chosen due to:

- Low memory footprint
- Single artefact binary (with cross platform, multi arch build support), so no installation deployment dependencies

The project consists of a CLI command , which starts a long running daemon process. When running, the daemon regularly polls for process information (pid, name, memory usage) with an asynchronous background task and sends datapoints to statds.

Currently two metrics are sent,  for example:

- `legendary_metrics.memory_virtual_memory_size`: virtual memory size
- `legendary_metrics.memory_resident_set_size`: resident set size 

Current hostname, pid, process name are sent as tags:

- statds:
    ```
    legendary_metrics.memory_resident_set_size:37220352|g|#hostname:mp10.local,pid:4219,process_name:AppleSpelegendary_metrics.memory_virtual_memory_size:4436819968|g|#hostname:mp10.local,pid:4180,process_name:cupsd

    legendary_metrics.memory_virtual_memory_size:4407762944|g|#hostname:mp10.local,pid:4193,process_name:storeinstalld
    ```

- prometheus:
    ```
    legendary_metrics_memory_resident_set_size{exporter="statsd",hostname="mp10.local",instance="statsd_exporter:9102",job="statsd_exporter",pid="10068",process_name="plugin-container"}	

    legendary_metrics_memory_virtual_memory_size{exporter="statsd",hostname="mp10.local",instance="statsd_exporter:9102",job="statsd_exporter",pid="1",process_name="launchd"}	
    ```

The service is built on top of [Cobra](https://github.com/spf13/cobra)/[Viper](https://github.com/spf13/viper) for follow best practices of [12 factor](https://12factor.net). CLI command for starting the service allows configuraton based on CLI params, environment variables and configuration files.

Funcionality is split across several Golang packages, also aiming at increased testability by encapsulating features:

- `agent`: core metrics collection agent service which handles PID file locking, polling and metrics reporting
- `cmd`: [Cobra](https://github.com/spf13/cobra) based CLI commands. Only has one root command, which handles CLI params and agent initialisaton
- `collectors`: data point collectors. Has one collector based on [gopsutil](https://github.com/shirou/gopsutil/) for getting a list of current local running processes.
- `config`: contains a data structure for holding agent runtime configuration (as read from Cobra).
- `scheduler`: Contains background job schedulers based on [go-cron](https://github.com/go-co-op/gocron) for implementing blocking and immediate polling:
    - `BlockingTaskScheduler` blocks current process and executes metrics handling at a configurable interval in the background
    - `NonBlockingTaskScheduler` executes all the scheduled jobs as soon as the scheduler is started, for unit testing purposes

## Agent configuration

The agent service CLI allows several configuration points, as command flags:

```
legendary-metrics-agent --help
Start metrics agent

Usage:
  legendary-metrics-agent [flags]

Flags:
      --config string                  config file (default is $HOME/.legendary-metrics-agent.yaml)
  -h, --help                           help for legendary-metrics-agent
      --metrics-polling-interval int   Polling interval for metrics in seconds (default 1)
      --statsd-client-prefix string    Statsd client prefix (optional) (default "legendary_metrics.")
      --statsd-server-address string   statsd UDP address (default "localhost:9125")
```

Environment variables can also be used. Available environment names are prefixed with `LEGENDARY_METRICS_AGENT_`, followed by the uppercased CLI param. Hiphens are replaced with underscores, ex: `LEGENDARY_METRICS_AGENT_STATSD_SERVER_ADDRESS`.

## Local development

### Requirements

- [Golang](https://golang.org)
- [golint](https://github.com/golang/lint) for improved linting
- [docker-compose](https://docs.docker.com/compose/): A Docker Compose configuration file is available to easily run a local stack with statds, Prometheus and prometheus-statds-exporter.

Make support exists for automating development tasks.

To start the local development monitoring stack, run:

    $ docker-compose up

The following services will be available:

- statds on udp `localhost:9125` (which exports metrics to local prometheus container service)
- statsd web interface at <http://localhost:9102>
- Prometheus web interface at <http://localhost:9090>

## Agent provisoning to hosts

### Requirements

- Ansible and Python

    ```
    $ pip3 install ansible==2.8
    ```
- [Vagrant](https://www.vagrantup.com)

### Design

In `ansible`, there is a playbook and a role that provisions a local Vagrant VM.
The local project binary is built locally and copied to the VM.
The agent is then started by Ansible with systemd :

```
vagrant@vagrant:~$ sudo systemctl status legendary-metrics-agent
● legendary-metrics-agent.service - Service for collecting per process used memory of a Linux host
   Loaded: loaded (/etc/systemd/system/legendary-metrics-agent.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2020-11-16 00:17:16 UTC; 8s ago
 Main PID: 5263 (legendary-metri)
    Tasks: 4 (limit: 503)
   CGroup: /system.slice/legendary-metrics-agent.service
           └─5263 /usr/local/bin/legendary-metrics-agent-linux

Nov 16 00:17:16 vagrant systemd[1]: Started Service for collecting per process used memory of a Linux host.
Nov 16 00:17:17 vagrant legendary-metrics-agent-linux[5263]: Polling metrics
Nov 16 00:17:18 vagrant legendary-metrics-agent-linux[5263]: Polling metrics
Nov 16 00:17:19 vagrant legendary-metrics-agent-linux[5263]: Polling metrics
Nov 16 00:17:20 vagrant legendary-metrics-agent-linux[5263]: Polling metrics
Nov 16 00:17:21 vagrant legendary-metrics-agent-linux[5263]: Polling metrics
Nov 16 00:17:22 vagrant legendary-metrics-agent-linux[5263]: Polling metrics
Nov 16 00:17:23 vagrant legendary-metrics-agent-linux[5263]: Polling metrics
Nov 16 00:17:24 vagrant legendary-metrics-agent-linux[5263]: Polling metrics
```

To test locally:

```
# Build the linux binary
$ make build-linux

# Start vagrant VM
$ vagrant up

```

The ansible provisioner should run, if not:

```
$ vagrant provision
```