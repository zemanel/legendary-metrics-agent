#
# Utilities for local development
#

.PHONY: build build-linux test lint vet check

build:
	go build -o bin/legendary-metrics-agent

# Build binary inside ansible role
build-linux:
	env GOOS=linux GOARCH=386 go build -o ansible/roles/zemanel.legendary-metrics-agent/files/legendary-metrics-agent-linux

lint:
	golint ./...

vet:
	go vet ./...

test:
	go test -race -v ./...
