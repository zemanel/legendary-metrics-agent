package agent

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/zemanel/legendary-metrics-agent/collectors"
	"gitlab.com/zemanel/legendary-metrics-agent/config"
	"gitlab.com/zemanel/legendary-metrics-agent/scheduler"
)

func Test_startAgentPidFile(t *testing.T) {

	agentConfig := config.AgentConfig{
		StatsdClientPrefix:  "test",
		StatsdServerAddress: "localhost:9125",
	}

	b := bytes.NewBufferString("")

	agent, err := New(&agentConfig, b, scheduler.NewNonBlockingTaskScheduler(), collectors.NewGoPsUtilProcessInfoCollector())
	assert.NoError(t, err)

	agent.Start()

	agent.Stop()
}
