package agent

import (
	"fmt"
	"io"
	"os"

	"github.com/DataDog/datadog-go/statsd"
	"github.com/shirou/gopsutil/v3/process"

	"gitlab.com/zemanel/legendary-metrics-agent/collectors"
	"gitlab.com/zemanel/legendary-metrics-agent/config"
	"gitlab.com/zemanel/legendary-metrics-agent/scheduler"
)

// Agent Metrics collection agent service
type Agent struct {
	configuration        *config.AgentConfig
	statsdClient         *statsd.Client
	stdOut               io.Writer
	taskScheduler        scheduler.TaskScheduler
	processInfoCollector collectors.ProcessInfoCollector
}

// Start Start the agent
func (agent *Agent) Start() (err error) {
	agent.taskScheduler.RunAtIntervalSeconds(agent.configuration.MetricsPollingInterval, agent.pollMetrics)
	agent.taskScheduler.Start()
	fmt.Println("Agent started")
	return
}

// Stop stop the agent
func (agent *Agent) Stop() (err error) {
	if err != nil {
		return
	}
	fmt.Println("Agent stopping")
	return
}

// pollMetrics gets process info and sends data points to statds
func (agent *Agent) pollMetrics() {
	fmt.Println("Polling metrics")

	processes, err := agent.processInfoCollector.GetRunningProcesses()
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, process := range processes {
		agent.pushProcessDataPoints(process)
	}

}

// pushProcessDataPoints Pushes datapoints for process
func (agent *Agent) pushProcessDataPoints(process *process.Process) {
	v, err := process.MemoryInfo()
	if err != nil {
		fmt.Println("Error", err)
		return
	}

	// Send process memory metrics
	processName, _ := process.Name()

	tags := []string{
		fmt.Sprintf("pid:%v", process.Pid),
		fmt.Sprintf("process_name:%v", processName),
	}
	agent.statsdClient.Gauge("memory_resident_set_size", float64(v.RSS), tags, 1)
	agent.statsdClient.Gauge("memory_virtual_memory_size", float64(v.VMS), tags, 1)
}

// New Instantiates and returns a new agent instance
func New(agentConfiguration *config.AgentConfig, stdOut io.Writer, taskScheduler scheduler.TaskScheduler, processInfoCollector collectors.ProcessInfoCollector) (*Agent, error) {

	statsdClient, err := statsd.New(agentConfiguration.StatsdServerAddress)

	// Configure namespace if available
	if agentConfiguration.StatsdClientPrefix != "" {
		statsdClient.Namespace = agentConfiguration.StatsdClientPrefix
	}

	// Send hostname tag on every metric
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	statsdClient.Tags = append(statsdClient.Tags, fmt.Sprintf("hostname:%s", hostname))

	if err != nil {
		// error initialising client
		return nil, err
	}

	agent := &Agent{
		configuration:        agentConfiguration,
		statsdClient:         statsdClient,
		stdOut:               stdOut,
		taskScheduler:        taskScheduler,
		processInfoCollector: processInfoCollector,
	}
	return agent, nil
}
